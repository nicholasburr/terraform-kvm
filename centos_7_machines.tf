resource "libvirt_volume" "centos_7_qcow2" {
  for_each = toset(var.centos_7_machines)
  name   = each.value
  source = "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2"
  format = "qcow2"
}

resource "libvirt_domain" "domain_centos_7" {
  for_each = toset(var.centos_7_machines)
  name   = each.key
  memory = "512"
  vcpu   = 1
  qemu_agent = "true"
  cloudinit = libvirt_cloudinit_disk.commoninit.id
  autostart = true
  cpu = {
  mode = "host-passthrough"
  }
  network_interface {
    bridge = "bridge0"
    wait_for_lease = "true"
  }
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }
  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }
  disk {
    volume_id = libvirt_volume.centos_7_qcow2[each.key].id
  }
  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }
}

resource "ansible_host" "centos_7_ansible_host" {
  for_each = toset(var.centos_7_machines)
  inventory_hostname = libvirt_domain.domain_centos_7[each.key].name
  groups = ["centos8"]
  vars = {
      ansible_user = "nburr"
      ansible_host = libvirt_domain.domain_centos_7[each.key].network_interface[0].addresses[0]
  }
}

terraform {
  required_version = ">= 0.12"
}
