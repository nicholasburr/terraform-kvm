resource "libvirt_volume" "centos_8_qcow2" {
  for_each = toset(var.centos_8_machines)
  name   = each.value
  source = "https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.1.1911-20200113.3.x86_64.qcow2"
  format = "qcow2"
}

resource "libvirt_domain" "domain_centos_8" {
  for_each = toset(var.centos_8_machines)
  name   = each.key
  memory = "512"
  vcpu   = 1
  qemu_agent = "true"
  cloudinit = libvirt_cloudinit_disk.commoninit.id
  network_interface {
    bridge = "bridge0"
    wait_for_lease = "true"
  }
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }
  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }
  disk {
    volume_id = libvirt_volume.centos_8_qcow2[each.key].id
  }
  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }
}

resource "ansible_host" "centos_8_ansible_host" {
  for_each = toset(var.centos_8_machines)
  inventory_hostname = libvirt_domain.domain_centos_8[each.key].name
  groups = ["centos8"]
  vars = {
      ansible_user = "nburr"
      ansible_host = libvirt_domain.domain_centos_8[each.key].network_interface[0].addresses[0]
  }
}

terraform {
  required_version = ">= 0.12"
}
