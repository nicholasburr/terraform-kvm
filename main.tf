provider "libvirt" {
  uri = "qemu:///system"
}


######   Virtual Machines   #######
# Each variable controles the name/# of systems per VM type.
variable "centos_8_machines" {
  description = "CentOS 8 virtual machines"
  type        = list(string)
  default     = ["centos8-01", "centos8-02"]
}

variable "centos_7_machines" {
  description = "CentOS 7 virtual machines"
  type        = list(string)
  default     = ["centos7-01"]
}


######   Cloud Init Data   #######
# This is able to be used for all images.
data "template_file" "user_data" {
  template = file("${path.module}/config-files/cloud_init.cfg")
}

data "template_file" "network_config" {
  template = file("${path.module}/config-files/network_config.cfg")
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "commoninit.iso"
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network_config.rendered
}
